# Code Challenge for Citi Client

## What you will need:
- Gradle 8.1.1

- Java 11

## Description
Api for getting synonyms from a word (maximum 2) in english. 
As reference, it is taken the wordsapi general api (the site's docs were not updated so, I obtained the real doc looking into the platform)

## What you will find:
- Api Working (Run it with the command:  "gradle bootRun")


- Unit testing with 71% coverage 
(Run the tests with the command "./gradlew clean build", and the open jacoco: http://localhost:63342/synonynms/build/jacocoHtml/index.html)


- Open Api Swagger Docs (Run the project and then open http://localhost:8080/swagger-ui/index.html#/word-controller/getSynonyms)

## Authors and acknowledgment
Developed By Diana Sanchez

## Notes
For newer Spring Boot versions it is common to use WebClient instead of RestTemplate.
in some cases, the non-blocking approach uses much fewer system resources compared to the blocking one. 
So, WebClient is a preferable choice in those cases. In this case I used RestTemplate.

## Thanks for reading!
