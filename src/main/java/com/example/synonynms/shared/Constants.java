package com.example.synonynms.shared;

public class Constants {

    /* It Could be in yml or properties file as well*/
    public static final String ENCRYPTED = "8cfdb189e7229b9bea9507bdee58bebcaeb02a0931f896b8";

    /* This param is required to call the Api,it was taken from the original website request that is working,
     but it's not working like in the documentation, if I change it to dynamic bye generating Date or if I change a minute, it doesn't work */
    public static final String REQUIRED_DATE = "2023-08-04T21:05:07.171Z";
}
