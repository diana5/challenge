package com.example.synonynms.mapper;

import com.example.synonynms.dto.SynonymsDTO;
import com.example.synonynms.dto.WordsDTO;
import org.springframework.stereotype.Component;

@Component
public class WordsDTOToSynonymsDTO {
    SynonymsDTO synonyms = new SynonymsDTO();
    public SynonymsDTO toSynonymsDTO(WordsDTO words) {
        synonyms.setSynonyms(words.getSynonyms());
        return synonyms;
    }
}
