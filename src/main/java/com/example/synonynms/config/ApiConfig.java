package com.example.synonynms.config;

import org.springframework.boot.web.client.RestTemplateBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.web.client.RestTemplate;

import java.time.Duration;

public class ApiConfig {

    @Bean
    public RestTemplate restTemplate(RestTemplateBuilder builder) {

        return builder
                .setConnectTimeout(Duration.ofMillis(3000))
                .setReadTimeout(Duration.ofMillis(3000))
                .build();
    }

    /*
    public WordsDTO getSynonyms(String name) {
        return this.restTemplate.getForObject("https://www.wordsapi.com/mashape/words/hello/synonyms?when=2023-08-04T21:05:07.171Z&encrypted=8cfdb189e7229b9bea9507bdee58bebcaeb02a0931f896b8", WordsDTO.class);
    } */
}
