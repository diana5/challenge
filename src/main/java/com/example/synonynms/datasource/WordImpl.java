package com.example.synonynms.datasource;

import com.example.synonynms.dto.WordsDTO;
import com.example.synonynms.shared.Constants;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import java.util.NoSuchElementException;
import java.util.Optional;

@Service
public class WordImpl {
    Logger logger = LoggerFactory.getLogger(WordImpl.class);

    private final RestTemplate restTemplate;
    @Value("${api.host.url}")
    private String url;

    public WordImpl(RestTemplate restTemplate) {
        this.restTemplate = restTemplate;
    }
    public WordsDTO getWordsApi(String word) {
        logger.info("Entered 'getWordsApi' on WordImpl ");
       return Optional.ofNullable(this.restTemplate.getForObject(url + word + "/synonyms?when=" + Constants.REQUIRED_DATE + "&encrypted=" + Constants.ENCRYPTED, WordsDTO.class))
                       .orElseThrow(NoSuchElementException::new);
        }}
