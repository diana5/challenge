package com.example.synonynms.controller;

import com.example.synonynms.datasource.WordImpl;
import com.example.synonynms.dto.ResponseDTO;
import com.example.synonynms.mapper.WordsDTOToSynonymsDTO;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.media.Content;
import io.swagger.v3.oas.annotations.media.Schema;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.responses.ApiResponses;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;


@RestController
@RequestMapping("/words/v1")
public class WordController {
    Logger logger = LoggerFactory.getLogger(WordController.class);

    private  final WordsDTOToSynonymsDTO wordsDTOToSynonymsDTO;
    private final WordImpl wordImpl;

    ResponseDTO responseDTO = new ResponseDTO();

    public WordController(WordsDTOToSynonymsDTO wordsDTOToSynonymsDTO, WordImpl wordImpl) {
        this.wordsDTOToSynonymsDTO = wordsDTOToSynonymsDTO;
        this.wordImpl = wordImpl;

    }


    @Operation(summary = "Get the Synonyms List")
    @ApiResponses(value = {
            @ApiResponse(responseCode = "200", description = "Found a List of Synonyms for the given word",
            content = { @Content(mediaType = "application/json",
            schema = @Schema(implementation = ResponseDTO.class)) }),
            @ApiResponse(responseCode = "400", description = "Bad request, No more than 2 words allowed"),
            @ApiResponse(responseCode = "500", description = "Server Error"),
            @ApiResponse(responseCode = "404", description = "Not Found"),
    })
    @GetMapping("synonyms/{word}")
    public ResponseEntity<ResponseDTO> getSynonyms(@PathVariable String word) {
        logger.info("Entered 'getSynonyms' on WordController");
        int wordsQuantity = word.split("\\s+|\n").length;
        if (wordsQuantity > 2) {
            responseDTO.setMessage("No more than 2 words allowed");
            responseDTO.setBody(null);
            return  ResponseEntity.badRequest().body(responseDTO);
        }
        else {
            responseDTO.setMessage("success");
            responseDTO.setBody(wordsDTOToSynonymsDTO.toSynonymsDTO(wordImpl.getWordsApi(word)));
            return  ResponseEntity.ok(responseDTO);
        }
    }

}
