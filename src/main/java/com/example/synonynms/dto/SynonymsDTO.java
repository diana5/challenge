package com.example.synonynms.dto;

import java.util.List;

public class SynonymsDTO {
    private List<String> synonyms;

    public List<String> getSynonyms() {
        return synonyms;
    }

    public void setSynonyms(List<String> synonyms) {
        this.synonyms = synonyms;
    }

    public SynonymsDTO(List<String> synonyms) {
        this.synonyms = synonyms;
    }

    public SynonymsDTO (){}
}
