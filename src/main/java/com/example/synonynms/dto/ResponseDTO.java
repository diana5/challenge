package com.example.synonynms.dto;

import io.swagger.v3.oas.annotations.media.Schema;

public class ResponseDTO {
    @Schema(
            description = "List of strings contents inside Words external Api, with a message",
            oneOf = {SynonymsDTO.class})
    private Object body;

    @Schema(
            description = "Message for the client",
            oneOf = {String.class})
    private String message;

    public Object getBody() {
        return body;
    }

    public void setBody(Object body) {
        this.body = body;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public ResponseDTO(Object body, String message) {
        this.body = body;
        this.message = message;
    }
    public ResponseDTO() {}
}
