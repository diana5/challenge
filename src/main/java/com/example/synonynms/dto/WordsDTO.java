package com.example.synonynms.dto;

import io.swagger.v3.oas.annotations.media.Schema;

import java.util.List;
public class WordsDTO {
    @Schema(
            description = "Word that is being searched",
            oneOf = {String.class})
    private String word;
    @Schema(
            description = "List of Synonyms Found",
            oneOf = {String.class})
    private List<String> synonyms;

    public String getWord() {
        return word;
    }

    public void setWord(String word) {
        this.word = word;
    }

    public List<String> getSynonyms() {
        return synonyms;
    }

    public void setSynonyms(List<String> synonyms) {
        this.synonyms = synonyms;
    }

    public WordsDTO(String word, List<String> synonyms) {

        this.word = word;
        this.synonyms = synonyms;
    }

    public WordsDTO() {}
}
