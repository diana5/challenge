package com.example.synonynms.datasource;

import com.example.synonynms.dto.WordsDTO;
import com.example.synonynms.shared.Constants;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.client.RestTemplate;

import java.util.ArrayList;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.Mockito.when;

@ExtendWith(MockitoExtension.class)
public class WordImplTest {

    private final String CORRECT_WORD = "hello";

    @Value("${api.host.url}")
    private String url;
    private final String URI =  url + CORRECT_WORD + "/synonyms?when=" + Constants.REQUIRED_DATE + "&encrypted=" + Constants.ENCRYPTED;

    @Mock
    RestTemplate restTemplate;
    @InjectMocks
    WordImpl wordImpl;
   @BeforeEach
    void init() {
        WordImpl wordImpl = new WordImpl(
                restTemplate
        );
    }
   @Test
   @DisplayName("SHOULD return '200' WHEN CalledGetWordsApi GIVEN words")
   void shouldReturn200_WhenCalledGetWordsApi_givenWord() {

       WordsDTO wordsDTOMock = new WordsDTO();
       wordsDTOMock.setWord(CORRECT_WORD);
       ArrayList<String> synonyms = new ArrayList<String>();
       synonyms.add("hi");
       wordsDTOMock.setSynonyms(synonyms);
       when(restTemplate.getForObject(URI,WordsDTO.class)).thenReturn(wordsDTOMock);
       WordsDTO response = wordImpl.getWordsApi(CORRECT_WORD);
       assertThat(wordsDTOMock.getWord()).isEqualTo(response.getWord());
       assertThat(wordsDTOMock.getSynonyms()).isEqualTo(response.getSynonyms());

   }

}
