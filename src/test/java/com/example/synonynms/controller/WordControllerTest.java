package com.example.synonynms.controller;

import com.example.synonynms.datasource.WordImpl;
import com.example.synonynms.dto.ResponseDTO;
import com.example.synonynms.dto.WordsDTO;
import com.example.synonynms.mapper.WordsDTOToSynonymsDTO;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.HttpServerErrorException;

import java.util.ArrayList;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.when;


@ExtendWith(MockitoExtension.class)
public class WordControllerTest {

    private final String WORD_MOCK = "turn into a martian";
    private final String NON_EXISTING_WORD_MOCK = "hellow";

    private final String EXISTING_WORD_MOCK = "hi";

    @Mock
    WordImpl wordImpl;
    WordController wordController;


    @BeforeEach
    void init() {
        wordController = new WordController(
                new WordsDTOToSynonymsDTO(),
                wordImpl
        );
    }

    @Test
    @DisplayName("SHOULD return '400' WHEN called 'synonyms' GIVEN more than two words")
    void shouldReturn400_WhenCalledWithSynonyms_givenMoreThanTwoWords(){

        ResponseEntity<ResponseDTO> response = wordController.getSynonyms(WORD_MOCK);

        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.BAD_REQUEST);
        assertThat(response.getBody().getMessage()).isEqualTo("No more than 2 words allowed");
    }

    @Test
    @DisplayName("SHOULD return '200' WHEN called with 'synonyms' GIVEN  existing word")
    void shouldReturn200_WhenCalledWithSynonyms_givenExistingWord() {

        WordsDTO wordsDTOMock = new WordsDTO();
        wordsDTOMock.setWord("hello");
        ArrayList<String> synonyms = new ArrayList<>();
        synonyms.add("hi");
        wordsDTOMock.setSynonyms(synonyms);
        when(wordImpl.getWordsApi(EXISTING_WORD_MOCK)).thenReturn(wordsDTOMock);
        ResponseEntity<ResponseDTO> response = wordController.getSynonyms(EXISTING_WORD_MOCK);

        assertThat(response.getStatusCode()).isEqualTo(HttpStatus.OK);
        assertThat(response.getBody().getMessage()).isEqualTo("success");

    }

    @Test
    @DisplayName("SHOULD Throw internal server error WHEN called with 'synonymsImpl' GIVEN Non existing word")
    void shouldThrowInternalServerError_WhenCalledWithSynonyms_givenNonExistingWord() {
        WordsDTO wordsDTOMock = new WordsDTO();
        wordsDTOMock.setWord(WORD_MOCK);
        wordsDTOMock.setSynonyms(new ArrayList<>());
        when(wordImpl.getWordsApi(NON_EXISTING_WORD_MOCK)).thenThrow(HttpServerErrorException.InternalServerError.class);

        assertThrows(HttpServerErrorException.InternalServerError.class,() -> wordController.getSynonyms(NON_EXISTING_WORD_MOCK));

    }
}
